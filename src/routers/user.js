const express = require('express');
const User = require('../models/user');
const auth = require('../middleware/auth');
const bcrypt = require('bcrypt');
const router = new express.Router();
const jwt = require("jsonwebtoken");
const nodemailer = require('nodemailer');

function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

async function generatedJWT(user) {
  const token = jwt.sign({_id: user._id.toString()}, 'qwerty');
  user.tokens = user.tokens.concat({token});
  await user.save();
  return token;
}

function generatedCode() {
  let code;
  for (let i = 0; ; i++) {
    code = Math.floor(Math.random() * 10000);
    if(code >= 1000 && code < 10000) {
      break;
    }
  }
  return code;
}

async function sendCode(email) {
  let testAccount = await nodemailer.createTestAccount();
  let code = generatedCode();

  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'korop.karas777@gmail.com',
      pass: 'karas2017'
    }
  });

  let info = await transporter.sendMail({
    from: '"Paintgram" <korop.karas777@gmail.com>', 
    to: email, 
    subject: 'Verification code',
    text: "We've sent you a verification code",
    html: '<p>You verification code: </p><b>' + code + '</b>',
  });

  console.log('Message sent: %s', info.messageId);

  return code;
}

router.post('/signup', async (req, res) => {
  try {
    if(validateEmail(req.body.email)) {
      var object = await User.findOne({ 'email': req.body.email });
      // if(object) {
      //   await User.deleteOne({ 'email': req.body.email });
      //   console.log('deleted');
      // }
      if(!object) {
        if(req.body.username.length >= 4) {
          if(req.body.password.length >= 8) {
            if(req.body.password == req.body.secondpassword) {
              let code = await sendCode(req.body.email);
              const user = new User({
                username: req.body.username,
                email: req.body.email,
                password: await bcrypt.hash(req.body.password, 8),
                isVerified: false,
                verificationcode: code,
              });
              
              try {
                await user.save();
                console.log(user);
                res.send({
                  completed: true,
                  email: req.body.email,
                });
              } catch (e) {
                res.status(500).send();
              }
            } else {
              res.send('Passwords did not match');
            }
          } else {
            res.send('Password must be 8 characters or more');
          }
        } else {
          res.send('Username must be 4 characters or more');
        }  
      } else {
        res.send('A user with this email already exists');
      }   
    } else {
      res.send('Incorrect email');
    }
  } catch (e) {
    res.status(404).send();
  }
});

router.post('/signupverify', async (req, res) => {
  try {
    var user = await User.findOne({ 'email': req.body.email });
    if(user) {
      if(user.verificationcode == req.body.code) {
        user.isVerified = true;
        user.verificationcode = null;
        const token = await generatedJWT(user);
        console.log(token);
        await user.save();
        console.log(user);

        res.send({
          completed: true,
          user: user,
          token: token,
        });
      } else {
        res.send('Incorrect code');
      }
    } else {
      res.send('User not found');
    }
  } catch (e) {
    res.status(404).send();
  }
});

router.post('/user/verify', async(req, res) => {
  try {
    const token = (req.body.token).replace(/['"]+/g, '');
    console.log(token);
    const decoded = jwt.verify(token, 'qwerty');  
    
    const user = await User.findOne({_id: decoded._id, 'tokens.token': token});
    console.log(user);
    if(user) {
      res.send({
        verify: true,
      });
    } else {
      res.send('Please authenticate');
    }
  } catch(e) {
    res.status(404).send('e');
  }  
})

module.exports = router;