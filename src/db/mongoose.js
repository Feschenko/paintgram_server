const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/paintgramdb', {
  useNewUrlParser: true,
  useCreateIndex: true
});