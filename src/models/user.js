const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcrypt');
const Scheme = mongoose.Schema

const User = new Scheme({
  username: {
    type: String,
    required: true,
    trim: true,
  },
  email: {
    type: String,
    validate(value) {
      if(validator.isEmail(value) != true) {
        throw new Error('Email is not valid')
      }
    },
  },
  password: {
    type: String,
    required: true,
    trim: true,
    validate(value) {
      value = value.toLowerCase();
      if(value.length < 7 || value == 'password') {
        throw new Error('Password is not valid');
      }
    },
  },
  secondpassword: {
    type: String,
    required: false,
    trim: true,
    validate(value) {
      value = value.toLowerCase();
      if(value.length < 7 || value == 'password') {
        throw new Error('Password is not valid');
      }
    },
  },
  isVerified: {
    type: Boolean,
    required: false,
  },
  avatar: {
    type: String,
    required: false,
  },
  verificationcode: {
    type: String,
    required: false,
  },
  tokens: [{
    token: {
      type: String,
      required: true,
    }
  }],
});

User.pre('save', async function(next) {
  const user = this;
  if (user.isModified('password'))
  {
    user.password = await bcrypt.hash(user.password, 8);
  }
  next();
})

User.methods.toJSON = function () {
  const user = this;
  const userObject = user.toObject();
  delete userObject.password;
  delete userObject.tokens;
  return userObject;
}

module.exports = mongoose.model('User', User);