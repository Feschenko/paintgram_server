const jwt = require("jsonwebtoken");
const User = require('../models/user');

const auth = async (req, res, next) => {
  console.log(req.body);
  try {
    console.log(req.body);
    const token = req.body.token;
    console.log({token});
    const decoded = jwt.verify(token, 'qwerty');
    console.log(decoded);
    const user = await User.findOne({_id: decoded._id, 'tokens.token': token});
    if(!user) {
      throw new Error();
    }
    req.user = user;
    req.token = token;
    next();
  } catch (err) {
    res.status(401).send({error: 'Please authenticate'});
  }
}

module.exports = auth;