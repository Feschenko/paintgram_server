const express = require('express');
require('./src/db/mongoose');
const userRouter = require('./src/routers/user');
// const taskRouter = require('./src/routers/task');

const request = require('request');
const path = require('path');
const http = require('http');
const { truncateSync } = require('fs');

const PORT = process.env.PORT || 3000;
const app = express();

app.use(express.urlencoded());
app.use(express.json());

const server = http.createServer(app);

server.listen(PORT, () => console.log(`Server running on port ${PORT}`));

app.use(userRouter);

app.get("/", async (req, res) => {
  try {
    res.send(true);
  }
  catch (e)
  {
    res.status(404).send();
  }
});